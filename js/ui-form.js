/**
 * COMMON FUNCTIONS THAT DEPENDS ON JQUERY-UI, JQUERY UI AUTO COMPLETE.
 */

var qtipOptionsDefault = {
	position: {
		my: 'top center',
		at: 'bottom center',
    type: 'fixed'
	},
  show: {
    solo: true
  },
	style: {
		tip: true,
    width: '170px',
		classes: 'ui-tooltip-orange'
	},
  events: {
    hide: function(event, api) {
      $(this).qtip('destroy');
    }
  }
};

//custom drop down menu
(function($) {
  function TvDropdown() {
    this.isField = true,
    this.down = false,
    this.disabled = false,
    this.cutOff = false,
    this.hasLabel = false,
    this.keyboardMode = false,
    this.nativeTouch = true,
    this.wrapperClass = 'dropdown',
    this.onChange = null;
  }

  TvDropdown.prototype = {
    constructor: TvDropdown,
    instances: {},
    init: function(domNode, settings) {
      var self = this;
      $.extend(self, settings);
      self.$select = $(domNode);
      self.id = domNode.id;
      self.options = [];
      self.$options = self.$select.find('option');
      self.isTouch = 'ontouchend' in document;
      self.$select.removeClass(self.wrapperClass + ' dropdown');
      if (self.$select.is(':disabled')) {
        self.disabled = true;
      };
      if (self.$options.length) {
        self.$options.each(function(i) {
          var $option = $(this);
          if ($option.is(':selected')) {
            self.selected = {
              index: i,
              title: $option.text()
            }
            self.focusIndex = i;
          };
          if ($option.hasClass('label') && i == 0) {
            self.hasLabel = true;
            self.label = $option.text();
            $option.attr('value', '');
          } else {
            self.options.push({
              domNode: $option[0],
              title: $option.text(),
              value: $option.val(),
              selected: $option.is(':selected')
            });
          };
        });
        if (!self.selected) {
          self.selected = {
            index: 0,
            title: self.$options.eq(0).text()
          }
          self.focusIndex = 0;
        };
        self.render();
      };
    },

    render: function() {
      var self = this;
      var touchClass = self.isTouch && self.nativeTouch ? ' touch' : '';
      var disabledClass = self.disabled ? ' disabled' : '';

      self.$container = self.$select.wrap('<div class = "' + self.wrapperClass + touchClass + disabledClass + '"><span class="old"/></div>').parent().parent();
      self.$active = $('<span class="selected">' + self.selected.title + '</span>').appendTo(self.$container);
      self.$button = $('<span class="button"/>').appendTo(self.$container);
      self.$scrollWrapper = $('<div><ul></div>').appendTo(self.$container);
      self.$dropDown = self.$scrollWrapper.find('ul');
      self.$form = self.$container.closest('form');
      $.each(self.options, function() {
        var option = this;
        var active = option.selected ? ' class = "active"' : '';
        self.$dropDown.append('<li' + active + '>' + option.title + '</li>');
      });
      self.$items = self.$dropDown.find('li');

      if (self.cutOff && self.$items.length > self.cutOff) self.$container.addClass('scrollable');

      self.getMaxHeight();

      if (self.isTouch && self.nativeTouch) {
        self.bindTouchHandlers();
      } else {
        self.bindHandlers();
      }
    },

    getMaxHeight: function() {
      var self = this;
      self.maxHeight = 0;

      for (i = 0; i < self.$items.length; i++) {
        var $item = self.$items.eq(i);
        self.maxHeight += $item.outerHeight();
        if (self.cutOff == i + 1) {
          break;
        };
      };
    },

    bindTouchHandlers: function() {
      var self = this;
      self.$container.on('click.Dropdown', function() {
        self.$select.focus();
      });
      self.$select.on({
        change: function() {
          var $selected = $(this).find('option:selected');
          var title = $selected.text();
          var value = $selected.val();

          self.$active.text(title);
          if (typeof self.onChange == 'function') {
            self.onChange.call(self.$select[0], {
              title: title,
              value: value
            });
          };
        },
        focus: function() {
          self.$container.addClass('focus');
        },
        blur: function() {
          self.$container.removeClass('focus');
        }
      });
    },

    bindHandlers: function() {
      var self = this;
      self.query = '';
      self.$container.on({
        'click.Dropdown' : function() {
          if (!self.down && !self.disabled) {
            self.open();
          } else {
            self.close();
          };
        },
        'mousemove.Dropdown': function() {
          if (self.keyboardMode) {
            self.keyboardMode = false;
          };
        }
      });

      $('body').on('click.Dropdown.' + self.id, function(e) {
        var $target = $(e.target);
        var classNames = self.wrapperClass.split(' ').join('.');

        if (!$target.closest('.' + classNames).length && self.down) {
          self.close();
        }
      });

      self.$items.on({
        'click.Dropdown': function() {
          var index = $(this).index();
          self.select(index);
          self.$select.focus();
        },
        'mouseover.Dropdown': function() {
          if (!self.keyboardMode) {
            var $i = $(this);
            $i.addClass('focus').siblings().removeClass('focus');
            self.focusIndex = $i.index();
          };
        },
        'mouseout.Dropdown': function() {
          if (!self.keyboardMode) {
            $(this).removeClass('focus');
          };
        }
      });

      self.$select.on({
        'focus.Dropdown': function() {
          self.$container.addClass('focus');
          self.inFocus = true;
        },
        'blur.Dropdown': function() {
          self.$container.removeClass('focus');
          self.inFocus = false;
        },
        'keydown.Dropdown': function(e) {
          if (self.inFocus) {
            self.keyboardMode = true;
            var key = e.keyCode;

            if (key == 38 || key == 40 || key == 32) {
              e.preventDefault();

              if(key == 38) {
                self.focusIndex--;
                self.focusIndex = self.focusIndex < 0 ? self.$items.length - 1 : self.focusIndex;
              } else if (key == 40) {
                self.focusIndex++;
                self.focusIndex = self.focusIndex > self.$items.length - 1 ? 0 : self.focusIndex;
              };

              if (!self.down) {
                self.open();
              };

              self.$items.removeClass('focus').eq(self.focusIndex).addClass('focus');
              if (self.cutOff) {
                self.scrollToView();
              };
              self.query = '';
            };
            if (self.down) {
              if (key == 9 || key == 27) {
                self.close();
              } else if (key == 13) {
                e.preventDefault();
                self.select(select.focusIndex);
                self.close();
                return false;
              } else if (key == 8) {
                e.preventDefault();
                self.query = self.query.slice(0, -1);
                self.search();
                clearTimeout(self.resetQuery);
                return false;
              } else if (key != 38 && key != 40) {
                var letter = String.fromCharCode(key);
                self.query += letter;
                self.search();
                clearTimeout(self.resetQuery);
              };
            };
          };
        },

        'keyup.Dropdown': function() {
          self.resetQuery = setTimeout(function() {
            self.query = '';
          }, 1200);
        }
      });

      self.$dropDown.on('scroll.Dropdown', function(e) {
        if (self.$dropDown[0].scrollTop >= self.$dropDown[0].scrollHeight - self.maxHeight) {
          self.$container.addClass('bottom');
        } else {
          self.$container.removeClass('bottom');
        };
      });

      if (self.$form.length) {
        self.$form.on('reset.Dropdown', function() {
          var active = self.hasLabel ? self.label : self.options[0].title;
          self.$active.text(active);
        });
      };
    },

    unbindHandlers: function() {
      var self = this;
      self.$container
        .add(self.$select)
        .add(self.$items)
        .add(self.$form)
        .add(self.$dropDown)
        .off('.Dropdown');
      $('body').off('.' + self.id);
    },

    open: function() {
      var self = this,
        scrollTop = window.scrollY || document.documentElement.scrollTop,
        scrollLeft = window.scrollX || document.documentElement.scrollLeft,
        scrollOffset = self.notInViewPort(scrollTop);

      self.closeAll();
      self.getMaxHeight();
      self.$select.focus();
      window.scrollTo(scrollLeft, scrollTop + scrollOffset);
      self.$container.addClass('open');
      self.$scrollWrapper.css('height', self.maxHeight + 'px');
      self.down = true;
    },

    close: function() {
      var self = this;
      self.$container.removeClass('open');
      self.$scrollWrapper.css('height', '0px');
      self.focusIndex = self.selected.index;
      self.query = '';
      self.down = false;
    },

    closeAll: function() {
      var self = this;
      var instances = Object.getPrototypeOf(self).instances;
      for(var key in instances) {
        var instance = instances[key];
        instance.close();
      };
    },

    select: function(index) {
      var self = this;
      if (typeof index === 'string') {
        index = self.$select.find('option[value=' + index + ']').index() - 1;
      };

      var option = self.options[index],
        selectIndex = self.hasLabel ? index + 1 : index;

      self.$items.removeClass('active').eq(index).addClass('active');
      self.$active.text(option.title);
      self.$select
        .find('option')
        .removeAttr('selected')
        .eq(selectIndex)
        .prop('selected', true)
        .parent()
        .trigger('change');

      self.selected = {
        index: index,
        title: option.title
      };

      self.focusIndex = i;
      if (typeof self.onChange === 'function') {
        self.onChange.call(self.$select[0], {
          title: option.title,
          value: option.value
        });
      };
    },

    search: function() {
      var self = this,
        lock = function(i) {
          self.focusIndex = i;
          self.$items.removeClass('focus').eq(self.focusIndex).addClass('focus');
          self.scrollToView();
        },
        getTitle = function(i) {
          return self.options[i].title.toUpperCase();
        };

      for (i = 0; i < self.options.length; i++) {
        var title = getTitle(i);
        if (title.indexOf(self.query) == 0) {
          lock(i);
          return;
        };
      };

      for (i = 0; i < self.options.length; i++) {
        var title = getTitle(i);
        if (title.indexOf(self.query) > -1) {
          lock(i);
          break;
        };
      };
    },

    scrollToView: function() {
      var self = this;
      if (self.focusIndex >= self.cutOff) {
        var $focusItem = self.$items.eq(self.focusIndex),
          scroll = ($focusItem.outerHeight() * (self.focusIndex + 1) - self.maxHeight);
        self.$dropDown.scrollTop(scroll);
      };
    },

    notInViewPort: function(scrollTop) {
      var self = this,
        range = {
          min: scrollTop,
          max: scrollTop + (window.innerHeight || document.documentElement.clientHeight)
        },
        menuBottom = self.$dropDown.offset().top + self.maxHeight;

      if (menuBottom >= range.min && menuBottom <= range.max) {
        return 0;
      } else {
        return (menuBottom - range.max) + 5;
      };
    },

    destroy: function() {
      var self = this;
      self.unbindHandlers();
      self.$select.unwrap().siblings().remove();
      self.$select.unwrap();
      delete Object.getPrototypeOf(self).instances[self.$select[0].id];
    },

    disable: function() {
      var self = this;
      self.disabled = true;
      self.$container.addClass('disabled');
      self.$select.attr('disabled', true);
      if(!self.down) self.close();
    },

    enable: function() {
      var self = this;
      self.disabled = false;
      self.$container.removeClass('disabled');
      self.$select.attr('disabled', false);
    }
  };

  var instantiate = function (domNode, settings) {
    domNode.id = !domNode.id ? 'Dropdown'+rand() : domNode.id;
    var instance = new TvDropdown();
    if (!instance.instances[domNode.id]) {
      instance.instances[domNode.id] = instance;
      instance.init(domNode, settings);
    };
  };

  var rand = function() {
    return ('00000'+(Math.random()*16777216<<0).toString(16)).substr(-6).toUpperCase();
  };

  $.fn.Dropdown = function() {
    var args = arguments,
      dataReturn = [],
      eachReturn;

    eachReturn = this.each(function() {
      if (args && typeof args[0] === 'string') {
        var data = TvDropdown.prototype.instances[this.id][args[0]](args[1], args[2]);
        if (data) dataReturn.push(data);
      } else {
        instantiate(this, args[0]);
      };
    });

    if (dataReturn.length) {
      return dataReturn.length > 1 ? dataReturn : dataReturn[0];
    } else {
      return eachReturn;
    };
  };

  $(function() {
    if (typeof Object.getPrototypeOf !== 'function') {
      Object.getPrototypeOf = function(object){
        return object.constructor.prototype;
      };
    };
  });
})(jQuery);

// custom Jquery plugin to popup a warning message for each field
(function($) {
	// private vars
	var numOfErrorMsgShown = 0;

  $.fn.extend({
    showWarnOnField: function(options) {
      options = $.extend({
        content: "Maaf ada kesalahan"
      }, qtipOptionsDefault,options);
      var that = this;
      that
          .qtip(options)
          .qtip('show');

      setTimeout(function() {
        that.qtip('hide');
      }, 2500 );

    }
  });
})(jQuery);

(function( $ ) {
  $.widget( "custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
      var self                 = this,
          currentCategory      = "",
          currentMasterAirport = '';

      $.each( items, function( index, item ) {
        if ( item.category != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
          currentCategory = item.category;
        }
        //determine whether this is the master airport;
        if ( item.isMasterAirport === true ) currentMasterAirport = item.airportCode;
        //delete the current master airport if it is not a master airport AND it doesn't have parent airport (hence subAirport)
        if ( item.isMasterAirport === false && item.parentAirport === '' ) currentMasterAirport = '';

        //this is a subairport if and only if
        //1. contains parent airport
        //2. its parent airport appear in the autocomplete list before.
        if ( item.parentAirport !== '' && item.parentAirport === currentMasterAirport ) item.isSubAirport = true;
        else item.isSubAirport = false;
        self._renderItem( ul, item );
      });
    },
    _renderItem: function (ul, item) {
      item.label = item.label.replace(
        new RegExp(
          "(?![^&;]+;)(?!<[^<>]*)(" +
          $.ui.autocomplete.escapeRegex(this.term) +
          ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"
      );
      var spacePrefix = item.isSubAirport === true ? '&nbsp;&nbsp;&nbsp;&nbsp;' : '';
      return $("<li></li>")
      .data("item.autocomplete", item)
      .append("<a>" + spacePrefix + item.label + "</a>")
      .appendTo(ul);
    }
  });
})(jQuery);

// custom Jquery UI combobox. This masks select a field into an autocomplete field
(function( $ ) {
  // Workaround for http://bugs.jqueryui.com/ticket/8439 - unbeforeunload overwritten on autocomplete()
  var autocomplete = $.fn.autocomplete;	// original fn
  $.fn.autocomplete = function() {	// intercept calls to $().autocomplete()
    var myCustomBeforeunloadListener = window.onbeforeunload;
    var result = autocomplete.apply(this, arguments);	// delegate
    window.onbeforeunload = myCustomBeforeunloadListener;	// reset
    return result;
  };

  $.widget( "ui.combobox", {
    options: {
      // TODO: put default message or anything here in the future (not important)
      validateValue: true,
      showMaxTopResult: 0,
      qtip: {},
      autocomplete: {}
    },
    _create: function() {
      var input,
          that = this,
          select = this.element.hide(),
          selected = select.children( ":selected" ),
          value = selected.val() ? selected.text() : "",
          defaultValue = value,
          wrapper = this.wrapper = $( "<span>" )
              .addClass( "ui-combobox" )
              .insertAfter( select ),
          qtipOption = {},
          autocompleteOption = {},
          validateValue = true,
          showMaxTopResult = 0;

      if ( this.options.qtip ) {
        qtipOption = $.extend(qtipOption, this.options.qtip);
      }
      if ( this.options.autocomplete ) {
        autocompleteOption = $.extend(autocompleteOption, this.options.autocomplete);
      }
      if ( this.options.validateValue ) {
        validateValue = this.options.validateValue;
      }
      if ( this.options.showMaxTopResult != 0 ) showMaxTopResult = this.options.showMaxTopResult;

      function removeIfInvalid(element) {
        if ( !validateValue ){
          return true;
        }
        var value = $( element ).val(),
            matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( value ) + "$", "i" ),
            valid = false;
        select.children( "option" ).each(function() {
          if ( $( this ).text().match( matcher ) ) {
            this.selected = valid = true;
            return false;
          }
        });
        if ( !valid ) {
          // remove invalid value, as it didn't match anything
          $( element ).val( defaultValue );
          if(value != '' && value != ' ')
            $( element ).showWarnOnField( $.extend({content: (value + ' tidak valid!')}, qtipOption));
          select.val( defaultValue );
          input.data( "autocomplete" ).term = "";
          return false;
        }
      }

      input = $( "<input>" )
          .attr("type", "text")
          .appendTo( wrapper )
          .val( value )
          .addClass( "ui-state-default ui-combobox-input" )
          .autocomplete($.extend({
            delay: 0,
            minLength: 0,
//            autoFocus: true,
            source: function( request, response ) {
                var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" ),
                    results = select.children( "option" ).map(function() {
                                var text = $( this ).text();
                                if ( this.value && ( !request.term || matcher.test(text) ) )
                                  return {
                                    label: text.replace(
                                      new RegExp(
                                        "(?![^&;]+;)(?!<[^<>]*)(" +
                                        $.ui.autocomplete.escapeRegex(request.term) +
                                        ")(?![^<>]*>)(?![^&;]+;)", "gi"
                                      ), "<strong>$1</strong>" ),
                                    value: text,
                                    option: this
                                  };
                              });
                if(showMaxTopResult != 0 ) results = results.slice(0, showMaxTopResult);
                response(results);
            },
            select: function( event, ui ) {
              ui.item.option.selected = true;
              that._trigger( "selected", event, {
                item: ui.item.option
              });
            },
            change: function( event, ui ) {
              if ( !ui.item ){
                return removeIfInvalid( this );
              }
            }
          },autocompleteOption))
//          .on('focus', function( event, ui ){
//            if ( !input.data("autocomplete").menu.element.is(":visible") ) { // IE issue workaround
//              $(this).autocomplete( "search", "" );  // set value to '' so that it forces the dropdown menu to show
//            }
//            return false;
//          })
          .on('click focus', function( event, ui ){
            $(this)
              .select()
              .autocomplete( "search", "" );  // set value to '' so that it forces the dropdown menu to show
            return false;
          });

      //overwrite autocomplete default behavior so that we can get the menu styled
      input.data("autocomplete")._renderItem = function(ul, item) {
        $item = $("<li>")
                  .data("item.autocomplete", item)
                  .append("<a>" + item.label + "</a>")
                  .appendTo(ul);
        if (this.element.val() === item.value) {  // try to highlight the  item if found
          var myInput = this,
              selectedItem = $item;
          setTimeout(function(){
            myInput.menu.activate( new $.Event("mouseover"), selectedItem );
          }, 10);
        }
        return $item;
      };
    },

    destroy: function() {
      this.wrapper.remove();
      this.element.show();
      $.Widget.prototype.destroy.call( this );
    }
  });
})( jQuery );

// field input validations
(function($) {
  $.fn.extend({
    forceNumericOnly: function(options) {
      options = $.extend({
        allowedKey: []
      },options);

      $(this)[0].allowedKeys = options.allowedKey;
      $(this).keydown(function(e){
        // Allow: backspace, delete, tab, escape, and enter
        if ( e.keyCode == 46 || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 27 || e.keyCode == 13 ||
           // Allow: Ctrl+A
          (e.keyCode == 65 && e.ctrlKey === true) ||
           // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
        }
        else {
          // OK for allowedKey
          for(var i = 0; i < this.allowedKeys.length; i++){
            if(e.keyCode == this.allowedKeys[i]) return;
          }

          // Ensure that it is a number and stop the keypress
          if (e.shiftKey || (e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105 )) {
            e.preventDefault();
          }
        }
      });

    }
  });
})(jQuery);

/**
 * Apply airport auto complete using the given data to the given element.
 *
 * @param jQueryElement $element Must be <input type="text">.
 * @param object airports From data.js.
 * @param object airportGroups From data.js.
 * @param object catCompleteOptions
 */
function applyAirportAutoComplete(
  $element,
  airports,
  airportGroups,
  catCompleteOptions
) {
  var airportList = [];
  if ($.isPlainObject(catCompleteOptions) == false) {
    catCompleteOptions = {};
  }
  var defaultCatCompleteOptions = {
    source: function(request, response) {
      var results = $.ui.autocomplete.filter(airportList, request.term).slice(0, 10);
      response(results);
    },
    change: function( event, ui ) {
      var airportName = $(this).val();
      if ( !ui.item ) {
        var matcher = new RegExp( "^" + $.ui.autocomplete.escapeRegex( airportName ) + "( - Semua Bandara)?$", "i" ),
            valid = false;
        for (var i=0; i < airportList.length; i++){
          if ( airportList[i].value.match( matcher ) ) {
            valid = true;
            break;
          }
        }

        if ( !valid ) {
          // remove invalid value, as it didn't match anything
          $( this ).val( "" );
          // What is this?
          //input.data( "autocomplete" ).term = "";
          return false;
        }
      }
    }
  };

  var parentAirportMap = {};

  for ( key in airportAreas ) {
    var airportIds = airportAreas[key].airportIds;
    for ( var i = 0; i < airportIds.length; ++i ) parentAirportMap[airportIds[i]] = key;
  }

  //init list for auto complete
  for (var i = 0; i < airportGroups.length; ++i) {
    var airportGroupId  = airportGroups[i].airportGroupId;
    var airportCountry  = airportGroupId > 10 ? airportGroups[i].name : 'Indonesia';

    var name = airportGroups[i].name;
    if (airportGroups[i].airportGroupId > 10) {
      var airportCountry = airportGroups[i].name;
    }
    //exclude Kota Populer (Domestik & Internasional)
    if ( (airportGroupId != 00) && (airportGroupId!= 10) ) {
      for ( var j = 0; j < airportGroups[i].airports.length; ++j ) {
        var airportCode      = airportGroups[i].airports[j],
            airportLocation  = getAirportOrArea(airportCode).location,
            airportName      = getAirportOrArea(airportCode).name;

        var labelAirportName = airportCode.length === 4 ? 'Semua Bandara' : airportName;
        var vLabel = airportLocation + ' (' + airportCode + ') - ' + labelAirportName;
        var semuaBandara = airportCode.length === 4 ? ' - Semua Bandara' : '';
        var groupObject = {
          label           : vLabel,
          realLabel       : vLabel,
          category        : airportGroups[i].name,
          airportCode     : airportCode,
          value           : airportLocation + ' (' + airportCode + ')' + semuaBandara,
          matchWith       : airportLocation + ' ' + airportCode + ' ' + airportCountry + '-' + airportName,
          parentAirport   : parentAirportMap[airportCode] != null ? parentAirportMap[airportCode] : '',
          isMasterAirport : airportCode.length == 4 ? true : false
        };
        airportList.push(groupObject);
      }
    }
  }

  $.extend(defaultCatCompleteOptions, catCompleteOptions);
  $element.catcomplete(defaultCatCompleteOptions);
}

/**
 * Parse value gotten from text input that has applyAirportAutoComplete()
 * applied (e.g. "Surabaya (SUB)") and returns the airport code (e.g. "SUB").
 *
 * @see applyAirportAutoComplete()
 * @param string value
 */
function parseAirportAutoCompleteValue(value) {
  var regex = /\(([A-Z]{3,4})\)/;
  var matches = value.match(regex);
  if ($.isArray(matches) == false || matches.length < 2) {
    return '';
  }
  return matches[1];
};
