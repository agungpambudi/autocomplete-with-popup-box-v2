//TODO Delete later
var urlParams = {};
var ap = 'NA', dt = 'NA', genericSearchType = 'NA', ps = '1.0.0';
var sourceAirportCode = 'NA', destinationAirportCode = 'NA', departureDate = 'NA', returningDate = 'NA', departureGroup = 'NA', returningGroup = 'NA', adultNum = '1', childNum = '0', infantNum = '0';
var autoFocusSearchField;
var isSeoPage = false;
var searchInformation = {};
var hasRangeSearch = false;

var searchContainerRes = new tv.i18n.ContentResource(searchContainerData, window.format);

var sourceAirportLabelStr = searchContainerRes.get("chooseDepartureAirport");
var destAirportLabelStr = searchContainerRes.get("chooseDestinationAirport");

var catCompleteOptions = {
  select: function(event, ui) {
    if ( event.keyCode == 13 || event.handleObj.type == 'click' ) {
      if ( $('#destinationAirportOrArea').val() == '' ) {
        $('#destinationAirportOrArea').focus();
        $('#destinationAirportOrArea').qtip('show');
      }
      else if ( $('#sourceAirportOrArea').val() == '' ) {
        $('#sourceAirportOrArea').focus();
        $('#sourceAirportOrArea').qtip('show');
      }
      else if ( $('#flightDate').val() == ''  ) {
        $('#flightDate').focus();
      }
    }

    var sourceAirport = $('#sourceAirportOrArea').val();
    var destinationAirport = $('#destinationAirportOrArea').val();
    var thisId = $(this).attr("id");

    if(thisId == 'sourceAirportOrArea')
      sourceAirport = ui.item.value;
    else if(thisId == 'destinationAirportOrArea')
      destinationAirport = ui.item.value;

    $('#rsSourceAirport').val(sourceAirport);
    $('#rsDestinationAirport').val(destinationAirport);
  },
  autoFocus: true,
  delay: 0
};

function resetField() {
  var SA = ((!getCookies().sourceAirport) ? "" : getCookies().sourceAirport);
  var DA = ((!getCookies().destinationAirport) ? "" : getCookies().destinationAirport);

  if (SA != null) $('#sourceAirportOrArea').val(SA);
  if (DA != null) $('#destinationAirportOrArea').val(DA);
  $('#flightDate').val('');
  $('#returnDate').val('');
  $('#adultPassenger').val('1');
  $('#childPassenger').val('0');
  $('#infantPassenger').val('0');
  $('#optionsRadios1').attr('checked', 'checked');
  $('#returnDateContainer').addClass('hidden');
}

function selectTab(id) //to select between tab for populer, domestic, and international airport
{
  if (id == 1)  {  //domestik

    $('.internationalTab').each(function(){
      $(this).removeClass('selected');
    });
    $('.domesticTab').each(function(){
      $(this).addClass('selected');
    });
    $('.internationalTable').each(function(){
      $(this).hide();
    });
    $('.domesticTable').each(function(){
      $(this).show();
    });
  }
  else if (id == 2)  {  //internasional

    $('.internationalTab').each(function(){
      $(this).addClass('selected');
    });
    $('.domesticTab').each(function(){
      $(this).removeClass('selected');
    });
    $('.internationalTable').each(function(){
      $(this).show();
    });
    $('.domesticTable').each(function(){
      $(this).hide();
    });
  }
}

function initSuggestionList() {
  if(autoFocusSearchField === true) {
    $('#sourceAirportOrArea').focus();
    $('#tvFirstSearchCol').addClass('active');
  }

  //mainSearchContainer
  var sourceAirport       = generateTable('sourceAirport'),
      destinationAirport  = generateTable('destinationAirport'),
      adjust = {x : 308, y : 10};
  if(typeof adjustSuggestionList !== "undefined") adjust = adjustSuggestionList;
  var generateOptions = function(airportList, titleText) {
    return {
      position  : {
        my        :'top center',
        at        :'bottom center',
        adjust    : adjust
      },
      style     : {
        width     : 950,
        tip       : false,
        classes   : 'ui-tooltip-light'
      },
      content   : {
        text      : airportList,
        title     : {
          text      : titleText,
          button    : $('<span></span>').addClass('fs-close-sum-icon ui-state-default')
        }
      },
      show      : {
        event     : 'click',
        delay     : 0
      },
      hide      : {
        event     : 'unfocus'
      }
    }
  };

  var sourceAirportEvents = {
    render  : function(event, api) {
      var self =  this;
      $('.sourceAirport').click(function() {
        $('#sourceAirportOrArea').val($(this).attr('value'));
        $('#rsSourceAirport').val($(this).attr('value'));
        $('#sourceAirportOrArea').qtip('hide');
        if ( $('#destinationAirportOrArea').val() == '' ) {
           $('#destinationAirportOrArea').focus();
           $('#destinationAirportOrArea').qtip('show');
        }
        else if ( $('#flightDate').val() == '' ) {
          $('#flightDate').focus();
        }

        var today = new Date();
        
      });
    },
    focus: function(event, api) {
      $('html, body').animate({
        scrollTop: $('#search-table').offset().top + 'px'
      }, 500);
    }
  };

  var destinationAirportEvents = {
    render  : function(event, api) {
      var self =  this;
      $('.destinationAirport').click(function() {
        $('#destinationAirportOrArea').val($(this).attr('value'));
        $('#rsDestinationAirport').val($(this).attr('value'));
        $('#destinationAirportOrArea').qtip('hide');
        if ( $('#sourceAirportOrArea').val() == '' ) {
          $('#sourceAirportOrArea').focus();
          $('#sourceAirportOrArea').qtip('show');
        }
        else if ( $('#flightDate').val() == '' ) {
          $('#flightDate').focus();
        }

        var today = new Date();
        
      });
    },
    focus: function(event, api) {
      $('html, body').animate({
        scrollTop: $('#search-table').offset().top + 'px'
      }, 500);
    }
  };

  $('#sourceAirportOrArea').qtip(generateOptions(sourceAirport, sourceAirportLabelStr));
  $('#sourceAirportOrArea').qtip('option', 'events', sourceAirportEvents);

  $('#destinationAirportOrArea').qtip(generateOptions(destinationAirport, destAirportLabelStr));
  $('#destinationAirportOrArea').qtip('option', 'events', destinationAirportEvents);
}

function initAutocomplete() {
  var $sourceAirportOrArea = $('#sourceAirportOrArea');
  var $destinationAirportOrArea = $('#destinationAirportOrArea');

  applyAirportAutoComplete(
    $sourceAirportOrArea,
    window.airports,
    window.airportGroups,
    catCompleteOptions
  );

  applyAirportAutoComplete(
    $destinationAirportOrArea,
    window.airports,
    window.airportGroups,
    catCompleteOptions
  );

  $sourceAirportOrArea.bind('keydown', function(e) {
    var keyCode = (e.keyCode? e.keyCode : e.which);
    if ( keyCode != 13 ) $sourceAirportOrArea.qtip('hide');
  });

  $destinationAirportOrArea.bind('keydown', function(e) {
    var keyCode = (e.keyCode? e.keyCode : e.which);
    if ( keyCode != 13 ) $destinationAirportOrArea.qtip('hide');
  });
}

function initDatepicker() {
  /* ~~ dates behavior ~~ */
  setMaxCharInField($("#flightDate"), 10);
  setMaxCharInField($("#returnDate"), 10);

  var flightDatePicker = {
    altField: "#alternateFlightDateHolder",
    onSelect: function( selectedDate ) {
      $( "#returnDate" ).datepicker( "option", "minDate", selectedDate );
      if ( !$('#returnDateContainer').hasClass('hidden') )
        setTimeout(function() {
          $('#returnDate').focus();
        } ,50);
      else if ( $('#sourceAirportOrArea').val() == '' ) $('#sourceAirportOrArea').focus();
      else if ( $('#destinationAirportOrArea').val() == '' ) $('#destinationAirportOrArea').focus();
      var flyDate = $('#flightDate').val().split('-');
      
    },
    onClose: function(dateText, inst) {
      if ( $('#flightDate').val() === '' ) $('#alternateFlightDate').text('').addClass('hidden');
    }
  };

  var returnDatePicker = {
    altField: "#alternateReturnDateHolder",
    onSelect: function( selectedDate ) {
      if ( $('#flightDate').val() == '' ) setTimeout(function() {
        $('#flightDate').focus();
      }, 50);
      else {
        clearHighlight();
        $('#tvThirdSearchCol').addClass('active');
      };
      var flyDate     = $('#returnDate').val().split('-');
      
    },
    onClose: function(dateText, inst) {
      if ( $('#returnDate').val() === '' ) $('#alternateReturnDate').text('').addClass('hidden');
    }
  };

}

function initBehaviour() {

  $('#optionsRadios1').change( function() {
    $('#returnDateContainer').addClass('hidden');
    $( "#flightDate" ).datepicker( "option", "maxDate", null );
    var hybridBtn = $('#searchHybridSubmit');
    if(hybridBtn) hybridBtn.click(startSearch({type: "FULL"}));
  });
  // TODO HIGH ABTEST HERE
  $('#optionsRadios2').change( function(){
    $('#returnDateContainer').removeClass('hidden');
    var hybridBtn = $('#searchHybridSubmit');
    if(hybridBtn) hybridBtn.click(startSearch({type: "FULL"})); // The new two way join search
  });

  //highlight inputbox when clicked
  $('#sourceAirportOrArea').click( function() {$('#sourceAirportOrArea').select();} );
  $('#destinationAirportOrArea').click( function() {$('#destinationAirportOrArea').select();} );

  //show suggestion list if inputbox is empty
  $('#sourceAirportOrArea').bind('keyup', function() {
    if ( $('#sourceAirportOrArea').val() === '' ) $('#sourceAirportOrArea').qtip('show');
  });
  $('#destinationAirportOrArea').bind('keyup', function() {
    if ( $('#destinationAirportOrArea').val() === '' ) $('#destinationAirportOrArea').qtip('show');
  });

  var hybridBtn = $('#searchHybridSubmit');
  $('#searchFlightSubmit').click(startSearch({type: "FULL"}));
  $('#searchSmartSubmit').click(startSearch({type: "FUN"}));
  if(hybridBtn) hybridBtn.click(startSearch({type: "FULL"}));

  //swap flight
  $('#swapFlightDestination').click(function() {
    var source      = $('#sourceAirportOrArea').val(),
        destination = $('#destinationAirportOrArea').val();

    //swap
    var temp = source;
    $('#sourceAirportOrArea').val(destination);
    $('#destinationAirportOrArea').val(temp);
  });

  $('#tvFirstSearchCol input').focus(function() {
    clearHighlight();
    $('#tvFirstSearchCol').addClass('active');
  });
  $('#tvSecondSearchCol input').focus(function() {
    clearHighlight();
    $('#tvSecondSearchCol').addClass('active');
  });
}

function initSEO() {
  if ( !isSeoPage ) return false;
  if ( typeof fromAirportCode != 'undefined' && typeof toAirportCode != 'undefined' ) {
    $('#sourceAirportOrArea').val(airports[fromAirportCode].location + ' (' + fromAirportCode + ')');
    $('#destinationAirportOrArea').val(airports[toAirportCode].location + ' (' + toAirportCode + ')');
  }
}

 //get date in the format of dd-mm-yyyy
 function parseDate(varDate) {
   return ('0' + varDate.getDate()).slice(-2) + '-' + ('0' + (varDate.getMonth()+1)).slice(-2) + '-' + varDate.getFullYear();
 }

 //convert date format from dd-mm-yyyy => mm/dd/yyyy;
 function convertDate(varDate) {
   var dateTokens = varDate.split('-');
   return dateTokens[1] + '/' + dateTokens[0] + '/' + dateTokens[2];
 }

var startSearch = function(searchType) {
  setCookies('sourceAirport',$('#sourceAirportOrArea').val(),365);
  setCookies('destinationAirport',$('#destinationAirportOrArea').val(),365);
  return function() {
    var isValid = validate();
    if ( !isValid ) return false;

    var departureDate       = $('#flightDate').val();
    var returningDate       = 'NA';

    if ( !$('#returnDateContainer').hasClass('hidden') && $('#returnDate').val != '' ) returningDate = $('#returnDate').val();

    var airport     = parseAirport($('#sourceAirportOrArea').val() + '.' + $('#destinationAirportOrArea').val());
    var date        = departureDate + '.' + returningDate;
    var passenger   = $('#adultPassenger').val() + '.' + $('#childPassenger').val() + '.' + $('#infantPassenger').val();

    if (searchType['type'] == 'GROUP') {
      date = date + '.' + searchType['grpName'];
    }

    var departureMonthDayYear = tv.helper.monthDayYear.parse(departureDate, 'd-m-y');
    var returningMonthDayYear = tv.helper.monthDayYear.parse(returningDate, 'd-m-y');
    var livepersonArr = [
      { scope:'page', name:'Origin', value:parseAirportAutoCompleteValue($('#sourceAirportOrArea').val()) },
      { scope:'page', name:'Destination', value:parseAirportAutoCompleteValue($('#destinationAirportOrArea').val()) },
      { scope:'page', name:'DepartureDay', value:departureMonthDayYear.day },
      { scope:'page', name:'DepartureMonth', value:departureMonthDayYear.month },
      { scope:'page', name:'DepartureYear', value:departureMonthDayYear.year },
      { scope:'page', name:'NumberAdults', value:$('#adultPassenger').val() },
      { scope:'page', name:'NumberChildren', value:$('#childPassenger').val() },
      { scope:'page', name:'NumberInfant', value:$('#infantPassenger').val() }
    ];
    if (goog.isDefAndNotNull(returningMonthDayYear)) {
      livepersonArr.push({ scope:'page', name:'ReturningDay', value:returningMonthDayYear.day });
      livepersonArr.push({ scope:'page', name:'ReturningMonth', value:returningMonthDayYear.month });
      livepersonArr.push({ scope:'page', name:'ReturningYear', value:returningMonthDayYear.year });
    }
    tv.liveperson.pushSend(livepersonArr);

    var queryStrData = {
      ap: airport,
      dt: date,
      ps: passenger
    };

    if (returningDate == 'NA') {
      if (searchType['type'] == 'FUN') {
        window.url.redirect('/funsearch', queryStrData);
      }
      else if (searchType['type'] == 'GROUP') {
        window.url.redirect('/groupsearch', queryStrData);
      }
      else if (searchType['type'] == 'FULL') {
        window.url.redirect('/fullsearch', queryStrData);
      }
    }
    else {
      // TODO AB TEST
      if (searchType['type'] == 'FUN') {
        window.url.redirect('/funtwosearch', queryStrData);
      }
      else if (searchType['type'] == 'GROUP') {
        window.url.redirect('/grouptwosearch', queryStrData);
      }
      else if (searchType['type'] == 'FULL') {
        window.url.redirect('/fulltwosearch', queryStrData);
      }
      else if (searchType['type'] == 'JOIN') {
        window.url.redirect('/jointwosearch', queryStrData);
      }
    }
    return false;
  };
};

/**
 * Put trivial function BELOW!!!
 */

function validate() {
  var a = $('#sourceAirportOrArea').val(),
      b = $('#destinationAirportOrArea').val(),
      c = $('#flightDate').val(),
      d = $('#returnDate').val();

  if ( parseAirport(a+".") == '.' ) {
    alert(searchContainerRes.get('unpickedSourceCity'));
    $('#sourceAirportOrArea').focus();
    $('#sourceAirportOrArea').qtip('show');
    return false;
  }
  if ( parseAirport(b+".") == '.' ) {
    alert(searchContainerRes.get('unpickedDestinationCity'));
    $('#destinationAirportOrArea').focus();
    $('#destinationAirportOrArea').qtip('show');
    return false;
  }
  if ( c == '' ) {
    alert(searchContainerRes.get('unpickedFlightDate'));
    $('#flightDate').focus();
    return false;
  }
  if ( $('#optionsRadios2').attr('checked') === 'checked' && d == '' ) {
    alert(searchContainerRes.get('unpickedReturnDate'));
    $('#returnDate').focus();
    return false;
  }
  if ( a === b ) {
    alert(searchContainerRes.get('destSourceCityEqual'));
    return false;
  }
  return true;
}

function clearHighlight() {
  $('#tvFirstSearchCol').removeClass('active');
  $('#tvSecondSearchCol').removeClass('active');
  $('#tvThirdSearchCol').removeClass('active');
}

function parseAirport(airport) {
  airportTokens = airport.split('.');
  var airport1 = parseAirportAutoCompleteValue(airportTokens[0]);
  var airport2 = parseAirportAutoCompleteValue(airportTokens[1]);
  return airport1 + '.' + airport2;
}

function generateTable(className) {

  var groupList = new Array();   //domestic airport
  $.each(airportGroups, function(index, value) {
    if((value.airportGroupId > 00) && (value.airportGroupId < 10))  {
      groupList.push(':' + value.name.toUpperCase() + ':' + value.airportGroupId);
      for ( var i = 0; i < value.airports.length; ++i ) {
        if (isAirportBelongsToAirportArea(value.airports[i], window.airportAreas)) {
          continue;
        }
        groupList.push(value.airports[i] + ':' + value.airportGroupId);
        //if ( value.airports[i] == 'JOG' && index != 0) groupList.push(' ');
      }
      groupList.push(' ');
    }
  })

  var groupList2 = new Array();  //international airport
  $.each(airportGroups, function(index, value) {
    if(value.airportGroupId > 10)  {
      groupList2.push(':' + value.name.toUpperCase() + ':' + value.airportGroupId);
      for ( var i = 0; i < value.airports.length; ++i ) {
        if (isAirportBelongsToAirportArea(value.airports[i], window.airportAreas)) {
          continue;
        }
        groupList2.push(value.airports[i] + ':' + value.airportGroupId);
        //if ( value.airports[i] == 'JOG' && index != 0) groupList.push(' ');
      }
      groupList2.push(' ');
    }
  })

  var groupList3 = new Array();  //populer airport
  $.each(airportGroups, function(index, value) {
    if ((value.airportGroupId == 00) || (value.airportGroupId == 10))   {
      groupList3.push(':' + value.name.toUpperCase() + ':' + value.airportGroupId);
      for ( var i = 0; i < value.airports.length; ++i ) {
        groupList3.push(value.airports[i] + ':' + value.airportGroupId);
        //if ( value.airports[i] == 'JOG' && index != 0) groupList.push(' ');
      }
      groupList3.push(' ');
    }
  })
  // groupList now contains => ":kota populer:00, CGK:00, DPS:00, UPG:00, etc, :Jawa:01, CGK:01, HLP:01, SUB:01"

  var columnSize = 1;
  var rowSize = Math.floor(groupList3.length / columnSize);
  var extraRow = groupList3.length % columnSize;
  var index;
  var colClass;
  var airportList ="";

  //only show populer airport
  airportList += '<div id="kotaPopuler">';
  airportList += '<table class="airportSelectTable" cellspacing="0">';
  airportList += '<colgroup>';
  for (var i = 0; i < columnSize; ++i){
    airportList += '<col class="airportName">';
  }
  airportList += '</colgroup>';
  for ( var i = 0; i < groupList3.length; ++i ) {
    colClass = '';
    var columnNow = i % columnSize;
    if ( columnNow == columnSize ) columnNow = 0; //if i == 0;
    if (columnNow == (columnSize-1) ) colClass = 'last'; // last column
    if ( columnNow == 0 ) airportList += '<tr>';
    index = (columnNow * rowSize) + Math.min(columnNow, extraRow) + Math.floor(i / columnSize);

    //insert td
    if ( groupList3[index] == ' ') {
      airportList += '<td class="padding '+colClass+'"></td>';
      continue;
    }
    var groupVal = groupList3[index].split(':');

    // Right now popular cities are hard coded. This needs to be refactored.
    // TODO HIGH: Refactor.
    if ( groupList3[index].substr(groupList3[index].length - 3, 3) == ':00' && groupList3[index].substr(0, 1) == ':' ) {
      //do something with the group header here
      airportList += '<div class="header ibu"><div class="airport-icon" id="kota-populer-icon"></div><span>' + groupVal[1].toLowerCase().capitalize() + '</span></div>';
    }

    if ( groupList3[index].substring(0,1) != ':' ) {
      if((groupVal[1] == '00') || (groupVal[1] == '10')) colClass += ' ibu'; //If header has Id 00 (kota populer)
      else if( airports[groupVal[0]] != null && airports[groupVal[0]].keyCity == true ) colClass += ' ibu'; // if it is important city
      var printName = getAirportOrArea(groupVal[0]).location;
      var semuaBandara = groupVal[0].length === 4 ? ' - Semua Bandara' : '';
      airportList += '<td class="'+colClass+'"><a class="' + className + '" href="javascript:void(0)" value="' +  getAirportOrArea(groupVal[0]).location + ' (' + groupVal[0] + ')' + semuaBandara + '">' + printName + '</a>';
    }
    airportList += '</td>';

    if ( columnNow == columnSize-1 ) airportList += '</tr>';
  }
  airportList += '</table></div>';

  columnSize = 4;
  rowSize = Math.floor(groupList.length / columnSize);
  extraRow = groupList.length % columnSize;

  var domestik = searchContainerRes.get('domestic');
  var internasional = searchContainerRes.get('international');

  //set up tab for domestic & international
  airportList += '<table class="tab" cellspacing="0">';
  airportList += '<tr>';
  airportList += '<td class="nolink" colspan=3></td>';
  airportList += '<td class="domesticTab selected" onclick="selectTab(1)"><div class="airport-icon" id="domestik-icon"></div><span>' + domestik + '</span></td>';
  airportList += '<td class ="internationalTab" onclick="selectTab(2)"><div class="airport-icon" id="internasional-icon"></div><span>' + internasional + '</span></td></tr>';
  airportList += '</table>';

  //only show domestic airport
  airportList += '<table class="airportSelectTable domesticTable" cellspacing="0">';
  airportList += '<tr><td class="padding last" colspan= "'+ columnSize +'"></td></tr>';

  airportList += '<colgroup>';
  for (var i = 0; i < columnSize; ++i){
    airportList += '<col class="airportName">';
  }
  airportList += '</colgroup>';
  for ( var i = 0; i < groupList.length; ++i ) {
    colClass = '';
    var columnNow = i % columnSize;
    if ( columnNow == columnSize ) columnNow = 0; //if i == 0;
    if (columnNow == (columnSize-1) ) colClass = 'last'; // last column
    if ( columnNow == 0 ) airportList += '<tr>';
    index = (columnNow * rowSize) + Math.min(columnNow, extraRow) + Math.floor(i / columnSize);

    //insert td
    if ( groupList[index] == ' ') {
      airportList += '<td class="padding '+colClass+'"></td>';
      continue;
    }
    var groupVal = groupList[index].split(':');
        if ( groupList[index].substring(0,1) == ':' ) {
          //do something with the group header here
          if((groupVal[2] == '00') || (groupVal[2] == '10')) colClass += ' ibu'; //If header has Id 00 (kota populer)
          var groupHeaderPrintName = groupVal[1];
          airportList += '<td class="header '+colClass+'">' + groupHeaderPrintName;
        }
        else {
          if((groupVal[1] == '00') || (groupVal[1] == '10')) colClass += ' ibu'; //If header has Id 00 (kota populer)
          else if(airports[groupVal[0]] != null && airports[groupVal[0]].keyCity == true) colClass += ' ibu'; // if it is important city
          var printName = getAirportOrArea(groupVal[0]).location;
          var semuaBandara = groupVal[0].length === 4 ? ' - Semua Bandara' : '';
          airportList += '<td class="'+colClass+'"><a class="' + className + '" href="javascript:void(0)" value="' + getAirportOrArea(groupVal[0]).location +' (' + groupVal[0] + ')' + semuaBandara + '">' + printName + '</a>';
        }
        airportList += '</td>';

    if ( columnNow == columnSize-1 ) airportList += '</tr>';
  }
  airportList += '</table>';

   //only show international airport
   columnSize = 4;
   rowSize = Math.floor(groupList2.length / columnSize);
   extraRow = groupList2.length % columnSize;

    airportList += '<table class="airportSelectTable internationalTable" cellspacing="0" style="display:none">';
    airportList += '<tr><td class="last" colspan= "'+ columnSize +'"></td></tr>';
    airportList += '<colgroup>';
    for (var i = 0; i < columnSize; ++i){
      airportList += '<col class="airportName">';
    }
    airportList += '</colgroup>';
    for ( var i = 0; i < groupList2.length; ++i ) {
      colClass = '';
      var columnNow = i % columnSize;
      if ( columnNow == columnSize ) columnNow = 0; //if i == 0;
      if (columnNow == (columnSize-1) ) colClass = 'last'; // last column
      if ( columnNow == 0 ) airportList += '<tr>';
      index = (columnNow * rowSize) + Math.min(columnNow, extraRow) + Math.floor(i / columnSize);

      //insert td
      if ( groupList2[index] == ' ') {
        airportList += '<td class="padding '+colClass+'"></td>';
        continue;
      }
      var groupVal = groupList2[index].split(':');
          if ( groupList2[index].substring(0,1) == ':' ) {
            //do something with the group header here
            if((groupVal[2] == '00') || (groupVal[2] == '10')) colClass += ' ibu'; //If header has Id 00 (kota populer)
            var groupHeaderPrintName = groupVal[1];
            airportList += '<td class="header '+colClass+'">' + groupHeaderPrintName;
          }
          else {
            if ((groupVal[1] == '00') || (groupVal[1] == '10')) colClass += ' ibu'; //If header has Id 00 (kota populer)
            else if(airports[groupVal[0]] != null && airports[groupVal[0]].keyCity == true) colClass += ' ibu'; // if it is important city
            var printName = getAirportOrArea(groupVal[0]).location;
            if (isAirportBelongsToAirportArea(groupVal[0], window.airportAreas)) {
              printName += ' (' + groupVal[0] + ')';
            }
            var semuaBandara = groupVal[0].length === 4 ? ' - Semua Bandara' : '';
            airportList += '<td class="'+colClass+'"><a class="' + className + '" href="javascript:void(0)" value="' +  getAirportOrArea(groupVal[0]).location + ' (' + groupVal[0] + ')' + semuaBandara + '">' + printName + '</a>';
          }
          airportList += '</td>';

      if ( columnNow == columnSize-1 ) airportList += '</tr>';
    }
    airportList += '</table>';

  return airportList;
}

function getSearchInformation() {

  var urlParams = {};
  var match,
      pl     = /\+/g,  // Regex for replacing addition symbol with a space
      search = /([^&=]+)=?([^&]*)/g,
      decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
      query  = window.location.search.substring(1);

  if (typeof window.query == 'undefined') {
    while ( match = search.exec(query) ) urlParams[decode(match[1])] = decode(match[2]);
    if ( typeof urlParams['ap'] != 'undefined' ) ap = urlParams['ap'];
    if ( typeof urlParams['dt'] != 'undefined' ) dt = urlParams['dt'];
    if ( typeof urlParams['ps'] != 'undefined' ) ps = urlParams['ps'];
  }
  else {
    // NOTE: No longer use query string, but trust value from server.
    if (typeof window.query.ap != 'undefined') window.ap = window.query.ap;
    if (typeof window.query.dt != 'undefined') window.dt = window.query.dt;
    if (typeof window.query.ps != 'undefined') window.ps = window.query.ps;
  }

  if (typeof searchType == 'undefined') return;
  else if ( searchType === 'NS1' ) { genericSearchType = 'FULL'; }
  else if ( searchType === 'JS2' ) { genericSearchType = 'FULL'; }
  else if ( searchType === 'NS2' ) { genericSearchType = 'FULL'; }
  else if ( searchType === 'SS1' ) { genericSearchType = 'FUN'; }
  else if ( searchType === 'SS2' ) { genericSearchType = 'FUN'; }
  else if ( searchType === 'GS1' ) { genericSearchType = 'GROUP'; }
  else if ( searchType === 'GS2' ) { genericSearchType = 'GROUP'; }

  if ( ap != 'NA' ) {
    var airportArr = ap.split('.');
    if ( airportArr.length === 2 ) {
      //get airports from airportAreas
      if ( airportAreas[airportArr[0]] != null ) sourceAirportCode = airportArr[0];
      if ( airportAreas[airportArr[1]] != null ) destinationAirportCode = airportArr[1];

      //otherwise if airportAreas doesn't exist
      if ( sourceAirportCode == 'NA' && airports[airportArr[0]] != null ) sourceAirportCode = airportArr[0];
      if ( destinationAirportCode == 'NA' && airports[airportArr[1]] != null ) destinationAirportCode = airportArr[1];
    }

    searchInformation.airportArr = airportArr;
  }

  if ( dt != 'NA' ) {
    var dateArr = dt.split('.');
    var datePattern = /((0[1-9])|([1-2][0-9])|(3[0-1]))\-((0[1-9])|(1[0-2]))\-(2[0-9][0-9][0-9])/;

    if ( datePattern.test(dateArr[0]) ) departureDate = dateArr[0];
    if ( dateArr[1] != null && datePattern.test(dateArr[1]) ) returningDate = dateArr[1];
    if ( dateArr[2] != null ) departureGroup = dateArr[2];
    if ( dateArr[3] != null ) returningGroup = dateArr[3];

    departureDate = changeToCurrentDate(departureDate, departureDate);
    if ( returningDate != 'NA' ) returningDate = changeToCurrentDate(returningDate, departureDate);

    searchInformation.dateArr = dateArr;
    searchInformation.departureDate = departureDate;
    searchInformation.returningDate = returningDate;
  }

  var psArr = ps.split('.');
  if (psArr.length === 3) {      //adult.child.infant , initialized with '1', '0', and '0'
    if (isNumber(psArr[0])) adultNum = Math.max(1, Math.min(7, (+psArr[0]))) + '';
    if (isNumber(psArr[1])) childNum = Math.min((7 - adultNum), (+psArr[1])) + '';
    if (isNumber(psArr[2])) infantNum = Math.min(adultNum, Math.min(7, (+psArr[2]))) + '';
  }

  var dateToFly = null;
  if (departureDate != 'NA') {
    dateToFly = Math.ceil((new Date(departureDate.substr(6,4)*1, departureDate.substr(3,2)*1-1, departureDate.substr(0,2)*1) - new Date().getTime()) / 86400000);
  }
  searchInformation.dateToFly = dateToFly;

  searchInformation.isTwoWay = (returningDate != 'NA');
  //window.monitor.log({className: "search", searchId: searchId, searchType: searchType, airport: searchInformation.airportArr, date: searchInformation.dateArr, adult: adultNum, child: childNum, infant: infantNum, dateToFly : searchInformation.dateToFly, date0 : searchInformation.departureDate, date1 : searchInformation.returningDate, isTwoWay : searchInformation.isTwoWay});
}

function fillAirportSearchBox(airportCode, targetSearchBox, targetLabel)
{
  var location = '';

  if ( airportAreas[airportCode] != null )
    location = airportAreas[airportCode].location;
  else if ( location == '' && airports[airportCode] != null )
    location = airports[airportCode].location;

  var airportFrom = location + ' (' + airportCode + ')';
  $(targetSearchBox).val(airportFrom);
  $(targetLabel).text(location);;
}

// Populate field with previous search spec
function populateField() {
  if ( sourceAirportCode != 'NA' )
    fillAirportSearchBox(sourceAirportCode, '#sourceAirportOrArea', '#searchResultFromLabel');

  if ( destinationAirportCode != 'NA' )
    fillAirportSearchBox(destinationAirportCode, '#destinationAirportOrArea', '#searchResultToLabel');

  if ( departureDate != 'NA' ) {
    $('#flightDate').val(departureDate);
    var flyDate = departureDate.split('-');
    
  }

  if ( returningDate == 'NA' ) {
  }
  else if ( returningDate != 'NA' ) {
    $('input:radio[value=option2]').attr('checked', true);
    $('#returnDateContainer').removeClass("hidden");
    $('#returnDate').val(returningDate);
    var flyDate = returningDate.split('-');
    
  }

  if(adultNum !== '1'){
    $('#adultPassenger').val(adultNum);
    $('#adultDisplay').text(adultNum + " Dewasa");
  }
  if(childNum !== '0'){
    $('#childPassenger').val(childNum);
    $('#childDisplay').text(childNum + " Anak");
  }
  if(infantNum !== '0'){
    $('#infantPassenger').val(infantNum);
    $('#infantDisplay').text(infantNum + " Bayi");
  }
}

function initPassengerFields(){
  var infoQtip = {
                 position : { my: 'bottom center',
                              at: 'top center'},
                 show: {
                   event     : 'mouseenter',
                   delay     : 0
                 },
                 hide: {
                   event: 'unfocus mouseleave'
                 },
                 style: {
                    classes: 'ui-tooltip-blue',
                    width: '220px'
                 }};
  $('#adultPassengerIcon').qtip($.extend(infoQtip, {content: "Dewasa: 12 tahun ke atas" }));
  $('#childPassengerIcon').qtip($.extend(infoQtip, {content: "Anak: 2 - 11 tahun", style: { width: '180px', classes: 'ui-tooltip-blue'} }));
  $('#infantPassengerIcon').qtip($.extend(infoQtip, {content: "Bayi: di bawah 2 tahun", position : {my: 'bottom right', at: 'top center'}, style: { width: '160px', classes: 'ui-tooltip-blue'} }));

  var adultS = $('#adultPassenger'),
      childS = $('#childPassenger'),
      infantS = $('#infantPassenger');

  var adultQtip = {position : { my: 'top center',
                                at: 'bottom center'}
                  };
  var childQtip = {position : { my: 'top center',
                                at: 'bottom center'}
                  };
  var infantQtip = {position : { my: 'top right',
                                at: 'bottom center'}
                  };
  adultS.combobox({qtip: adultQtip});
  childS.combobox({qtip: childQtip});
  infantS.combobox({qtip: infantQtip});


  var adultInput = $('#adultPassengerCntr').find('input').first(),
      childInput = $('#childPassengerCntr').find('input').first(),
      infantInput =  $('#infantPassengerCntr').find('input').first();

  adultInput.bind("autocompleteclose", function(event, ui){
          var noAdult = adultS.val()*1,
              noChildren = childS.val()*1,
              noInfant = infantS.val()*1,
              total = noAdult + noChildren;
          if(noInfant > noAdult){ //infant cannot be greater than adult
            adultS.val(noInfant);
            adultInput.val(noInfant);
            adultInput.showWarnOnField($.extend({content: searchContainerRes.get('psgAdultMoreThanBaby')}, adultQtip));
            return;
          }
          if(total > 7){ //total adult and children cannot be more than 7
            adultS.val(7-noChildren);
            adultInput.val(7-noChildren);
            adultInput.showWarnOnField($.extend({content: searchContainerRes.get('psgAdultChildMaxed', {num: 7})}, adultQtip));
            return;
          }
          return;
        });

  childInput.bind("autocompleteclose", function(event, ui){
          var noAdult = adultS.val()*1,
              noChildren = childS.val()*1,
              total = noAdult + noChildren;
          if(total > 7){ //total adult and children cannot be more than 7
            childS.val(7-noAdult);
            childInput.val(7-noAdult);
            childInput.showWarnOnField($.extend({content: searchContainerRes.get('psgAdultChildMaxed', {num: 7})}, childQtip));
            return;
          }
          return;
        });
  infantInput.bind("autocompleteclose", function(event, ui){
          var noAdult = adultS.val()*1,
              noInfant = infantS.val()*1;
          if(noInfant > noAdult){ //infant cannot be greater than adult
            infantS.val(noAdult);
            infantInput.val(noAdult);
            infantInput.showWarnOnField($.extend({content: searchContainerRes.get('psgAdultMoreThanBaby')}, infantQtip));
            return;
          }
          return;
        });

  $('#adultPassengerIcon').click(function(){
     adultInput.click();
  });
  $('#childPassengerIcon').click(function(){
     childInput.click();
  });
  $('#infantPassengerIcon').click(function(){
     infantInput.click();
  });
  $('#passengerNumber input').focus(function() {
    clearHighlight();
    if ( $('#tvThirdSearchCol').length == 0 ) $('#tvFirstSearchCol').addClass('active');
    else $('#tvThirdSearchCol').addClass('active');
  });
}

function frontPageSlider() {
  var timer = 1000;
  var transition = 400;
  var today = new Date();
  var imgHasLoaded = [];
  var indexPrev;
  var minIndex = 0;

  
  var destinationLink = window.destinationLink;

  // Front page did not gave us the arrays. Return immediately.
  if ($.isArray(destinationLink) == false) return;

  var sliderButtons = $('.tvFrontPageButtons');
  var $sliderContainer = $('.tvFrontPageSliderContainer');

  var imageCounter = 0;

  var timerCounter = timer;
  var imageCounterPrev = imageCounter;

  imageCounter++;

}

function changeToCurrentDate(varDate, varCompDate) {
  var dateNow  = new Date();
  var dateTemp = new Date(convertDate(varDate));

  //if selected date < current date
  if ( dateTemp.getTime() + 24*60*60*1000 < dateNow.getTime() ) return parseDate(dateNow);
  //if returning date < departure date
  if ( dateTemp.getTime() < new Date(convertDate(varCompDate)).getTime() ) return varCompDate;
  //else return selected date
  return varDate;
}

$(function() {
  if (typeof isInfoPenerbanganPage != 'undefined' && isInfoPenerbanganPage === true) {
    isSeoPage = true;
  }
  resetField();
  getSearchInformation();
  populateField();
  initSuggestionList();
  initAutocomplete();
  initDatepicker();
  initBehaviour();
  initPassengerFields();
  initSEO();
  
  frontPageSlider();
});


